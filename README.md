[![Build Status](https://gitlab.com/calevans/s3-podcast-log-cruncher/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/gitlab-ce/commits/master)

# s3-podcast-log-cruncher
A WordPress plugin for podcasts that are using WordPress and store their content on AWS S3. It crunches the log files created by S3 and updates the posts to which they.

-  0.5 MVP. It will read files in and archive them in the database.
-  0.6 Now with working WordPress Cron. :)
-  1.0 Newly refactored code. Everything working, all tests are green. Added the Top Downloads shortcode.

**This plugin requires PHP 7.2+**

## Top Downloads Page
To create a Top Downloads Page create a new page and add the shortcode
\[s3plc_top_episodes\]

Currently, there are 2 parameters
- **episode_count** The number of episodes to list. The default is 10.
- **desc_asc** ASC or DESC. This is the sort order for the list. The default is DESC.

## Version History

- 1.0.0 Initial working release

This is the first version used in production on [Voices of the ElePHPant](https://voicesoftheelehphant.com).

- 1.0.1 Clean and Green

All Unit Tests are passing. PHPCS passes with no errors. (Lots of warnings, but I'm ignoring those.)


(c) Cal Evans

All Rights Reserved
