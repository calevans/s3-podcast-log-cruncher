<?php

$_tests_dir = getenv('WP_TESTS_DIR');
if ( !$_tests_dir ) $_tests_dir = '/tmp/wordpress-tests-lib';

require_once $_tests_dir . '/includes/functions.php';

function _manually_load_plugin() {
	$_tests_dir = '/tmp/wordpress-tests-lib';
	require dirname( __FILE__ ) . '/../s3-podcast-log-cruncher.php';
	//require '/tmp/wordpress/wp-content/plugins/powerpress/powerpress.php';

// Update array with plugins to include ...
	$plugins_to_active = array(
		's3-podcast-log-cruncher/s3-podcast-log-cruncher.php'
	);

	update_option( 'active_plugins', $plugins_to_active );

}

tests_add_filter( 'muplugins_loaded', '_manually_load_plugin' );
define('S3PLC_TESTING', true);

require $_tests_dir . '/includes/bootstrap.php';

