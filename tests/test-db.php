<?php
/**
 * Created by PhpStorm.
 * User: cal
 * Date: 5/11/15
 * Time: 6:53 PM
 */

require_once 'S3PLCBaseTest.php';

use S3PLC\File;
use S3PLC\Options;
use S3PLC\DB;
use S3PLC\Bootstrap;
use S3PLC\LogCruncher;
use League\Flysystem\Adapter\Local;

class S3PLCTestDB extends S3PLCBaseTest {

    public function test_remove_record_id() {
        $dbManager = new DB();
        $new_record = $this->invokeMethod($dbManager,'init_record');
        $retrived_record = $this->invokeMethod($dbManager,'remove_record_id',[$new_record]);
        $this->assertFalse(isset($retrived_record['ID']));
        return;
    }


    public function test_create_table()
    {
        $dbManager = new DB();
        $optionManager = new Options();
        $optionManager->set_options($optionManager->get_options(true));
        $db = new DB();
        $wpdb = $this->invokeMethod($dbManager,'fetch_wpdb');

        // create the table
        remove_filter( 'query', array( $this, '_create_temporary_tables' ) );
        remove_filter( 'query', array( $this, '_drop_temporary_tables' ) );
        $this->invokeMethod($db, 'create_table');
        $this->assertTrue($db->sanity_check());
        $this->invokeMethod($db, 'destroy_table');
    }

    public function test_destroy_table() {
        $optionManager = new Options();
        $optionManager->set_options($optionManager->get_options(true));
        $dbManager = new DB();
        $wpdb = $this->invokeMethod($dbManager,'fetch_wpdb');

        // create the table and make sure it worked
        $this->invokeMethod($dbManager,'create_table');
        $this->assertTrue($dbManager->sanity_check());

        // destroy the table, verify, and verify the version is gone.
        $returnValue = $this->invokeMethod($dbManager,'destroy_table');
        $this->assertFalse($dbManager->sanity_check());
        $db_version = get_option('s3plc_db_version');
        $this->assertFalse($db_version);
    }

    public function test_init_record() {
        $db = new DB();
        $new_record = $this->invokeMethod($db,'init_record');
        $this->assertCount(20,$new_record);
        return;
    }

    public function test_record_store_and_retrieve() {
        // create the table
        $dbManager = new DB();
        $optionManager = new Options();
        $options = $optionManager->get_options();
        $this->invokeMethod($dbManager,'create_table');

        // Create a new record
        $new_record = $this->invokeMethod($dbManager,'init_record');
        $new_record['key']= '/test/my.mp3';

        // test store record
        $new_record_id = $this->invokeMethod($dbManager,'store_record',[$new_record]);
        $this->assertGreaterThan(0,$new_record_id);

        // test retrieve record
        $retrived_record = $this->invokeMethod($dbManager,'retrieve_record',[$new_record_id]);
        $this->assertCount(20,$retrived_record);
        $this->assertEquals($new_record_id,$retrived_record['ID']);
        $this->assertEquals($retrived_record['key'],$retrived_record['key']);

        // test second retrieve that should pull from memory
        $retrived_record = $this->invokeMethod($dbManager,'retrieve_record',[$new_record_id]);
        $this->assertEquals($new_record_id,$retrived_record['ID']);

        // test retrieving a non-existant record
        $retrieved_record = $this->invokeMethod($dbManager,'retrieve_record',[$new_record_id+1]);
        $this->assertNull($retrieved_record);

        // test storing an empty record
        $this->assertEquals(-1, $dbManager->store_record([]));
        $this->invokeMethod($dbManager,'destroy_table');
        return;
    }

    public function test_rollup_history() {

        $this->create_data_directory();
        $this->generate_test_data(60,-28);
        $this->generate_test_data(60,-32);

        $dbManager       = new DB();
        $optionsManager  = new Options();
        $filesManager    = new File();
        $bootstrap       = new Bootstrap(plugin_dir_path( __FILE__ ).'../');
        $log_cruncher    = new LogCruncher($optionsManager,$filesManager,$dbManager,$bootstrap);

        // create the table
        $options = $optionsManager->get_options();
        $this->invokeMethod($dbManager,'create_table');

        // limit the run to 100 files
        $options['process_count'] = 1000;
        $options['log_dir']       = $this->generated_data_dir;
        $adapter = new Local(__DIR__);
        $filesManager->get_flysystem($options,$adapter);
        $files   = $this->invokeMethod($filesManager,'fetch_log_files_list',[$options]);
        $this->invokeMethod($filesManager,'process_log_files',[$files,$options]);

        $options['days_to_keep_history'] = 0;
        $wpdb = $this->invokeMethod($dbManager,'fetch_wpdb');

        $counter = $this->invokeMethod($log_cruncher,'rollup_history',[$options,$wpdb, $dbManager]);
        $this->assertEquals(0,$counter);

        $options['days_to_keep_history'] = 30;
        $counter = $this->invokeMethod($log_cruncher,'rollup_history',[$options,$wpdb, $dbManager]);
        $this->assertEquals(1,$counter);
        //$this->commit_transaction(); // temporary so I can see the data.
        //die();

        $returnValue = $this->invokeMethod($dbManager,'destroy_table');
        $this->remove_data_directory();
    }

    public function test_add_download_count_to_a_post() {

        $this->create_data_directory();
        $post = array('post_status' => 'publish', 'post_title' => 'Audio:' . rand_str(), 'post_type' => 'post', 'post_content' => rand_str());
        // create the post
        $post_id = $this->factory->post->create($post);
        $media = "http://elephpant.s3.amazonaws.com/vote_126.mp3" .
            "\n39770015" .
            "\naudio/mpeg" .
            "\n" . 'a:3:{s:8:"duration";s:8:"00:16:30";s:8:"subtitle";s:35:"Interview with Michelangelo van Dam";s:6:"author";s:23:"Voices of the ElePHPant";}';
        update_post_meta($post_id, 'enclosure', $media);
        $this->generate_test_data(10,0,['key'=>'vote_126.mp3']);

        $dbManager       = new DB();
        $optionsManager  = new Options();
        $filesManager    = new File();
        $bootstrap       = new Bootstrap(plugin_dir_path( __FILE__ ).'../');
        $log_cruncher    = new LogCruncher($optionsManager,$filesManager,$dbManager,$bootstrap);

        $this->invokeMethod($dbManager,'create_table');

        $options = $optionsManager->get_options();
        $options['log_bucket']           = __DIR__;
        $options['log_dir']              = $this->generated_data_dir;
        $returnValue = $log_cruncher->main($options);

        $returnValue = $this->invokeMethod($dbManager, 'find_post_by_media', ['vote_126.mp3']);
        $this->assertEquals($post_id, $returnValue);

        query_posts('p='.$post_id);
        the_post();

        $content = apply_filters('the_content', get_the_content());

        $this->assertContains('<div class="s3plc_container">', $content);
        $pattern = '/<div class="s3plc_container"><span class="s3plc_text">Downloads: <\/span><span class="s3plc_downloads">(.*)<\/span><\/div>/';
        preg_match($pattern,$content, $matches);

        $this->assertCount(2,$matches);
        $this->assertEquals(10,$matches[1]);

        $this->invokeMethod($dbManager,'destroy_table');
        $this->remove_data_directory();

        return;
    }

}
