<?php
/**
 * Created by PhpStorm.
 * User: cal
 * Date: 5/9/15
 * Time: 11:53 AM
 */
require_once 'S3PLCBaseTest.php';

use S3PLC\File;
use S3PLC\Options;
use S3PLC\Db;
use S3PLC\LogCruncher;
use S3PLC\Bootstrap;

use League\Flysystem\Adapter\Local;

class S3PLCTestLogCruncher extends S3PLCBaseTest
{
    public function test_sanity_check_success() {
        $dbManager       = new DB();

        $optionsManager = new Options();
        $options = $optionsManager->get_options();
        $options['log_bucket']    = __DIR__;
        $options['log_dir']       = $this->generated_data_dir;
        $filesManager = new File();
        $filesManager->get_flysystem($options);
        $log_cruncher    = new LogCruncher(null, $filesManager);

        $this->invokeMethod($dbManager,'create_table');

        $returnValue = $this->invokeMethod($log_cruncher,'sanity_check');
        $this->assertTrue($returnValue);
        $this->invokeMethod($dbManager,'destroy_table');
        return;
    }

    public function test_main_sanity_check_failure()
    {
        $adapter = new Local(__DIR__);

        $log_cruncher    = new LogCruncher();
        $returnValue = $log_cruncher->main([], $adapter);
        $this->assertFalse($returnValue);
        return;
    }

    public function test_main_success() {
        $dbManager       = new DB();
        $optionsManager  = new Options();
        $filesManager    = new File();
        $bootstrap       = new Bootstrap(plugin_dir_path( __FILE__ ).'../');
        $log_cruncher    = new LogCruncher($optionsManager,$filesManager,$dbManager,$bootstrap);

        // create the table
        $this->create_data_directory();
        $this->generate_test_data(100,-28);
        $this->generate_test_data(100,-32);

        $options = $optionsManager->get_options();
        // create the table
        remove_filter( 'query', array( $this, '_create_temporary_tables' ) );
        remove_filter( 'query', array( $this, '_drop_temporary_tables' ) );
        $this->invokeMethod($dbManager, 'create_table');

        $options['log_bucket']    = __DIR__;
        $options['process_count'] = 75;
        $options['log_dir']       = $this->generated_data_dir;

        $returnValue = $log_cruncher->main($options);
        $schedule    = wp_next_scheduled( 's3plc_main');

        $this->assertGreaterThan(time(),$schedule);

        $this->assertEquals($options['process_count'],$returnValue);
        $this->invokeMethod($dbManager, 'destroy_table');
        return;
    }

    public function test_rollup_history_success()
    {
        $dbManager       = new DB();
        $optionsManager  = new Options();
        $filesManager    = new File();
        $bootstrap       = new Bootstrap(plugin_dir_path( __FILE__ ).'../');
        $log_cruncher    = new LogCruncher($optionsManager,$filesManager,$dbManager,$bootstrap);

        $this->create_data_directory();
        $this->invokeMethod($dbManager, 'create_table');

        $this->generate_test_data(500, -28);
        $this->generate_test_data(500, -32);

        $options = $optionsManager->get_options();
        $options['process_count']        = 0;
        $options['days_to_keep_history'] = 30;
        $options['log_bucket']           = __DIR__;
        $options['log_dir']              = $this->generated_data_dir;

        $returnValue = $log_cruncher->main($options);
        $wpdb = $this->invokeMethod($dbManager,'fetch_wpdb');

        $record_count = $wpdb->get_col('select count(*) from ' . $wpdb->prefix . 's3plc');
        $this->assertTrue($returnValue);
        $this->assertEquals(501, $record_count[0]);
        $this->invokeMethod($dbManager, 'destroy_table');

        return;
    }

    public function test_main_with_rollup_history_with_ignored_files() {

        $dbManager       = new DB();
        $optionsManager  = new Options();
        $filesManager    = new File();
        $bootstrap       = new Bootstrap(plugin_dir_path( __FILE__ ).'../');
        $log_cruncher    = new LogCruncher($optionsManager,$filesManager,$dbManager,$bootstrap);

        $this->create_data_directory();
        $this->generate_test_data(50, 30,['operation'=>'REST.HEAD.OBJECT']);
        $this->generate_test_data(50, 30,['operation'=>'REST.GET.BUCKET']);
        $this->generate_test_data(100,30,['status'=>206]);
        $this->generate_test_data(100);
        $this->invokeMethod($dbManager,'create_table');

        $options = $optionsManager->get_options();
        $options['log_bucket']    = __DIR__;
        $options['days_to_keep_history'] = 0;
        $options['log_dir']              = $this->generated_data_dir;

        $returnValue = $log_cruncher->main($options);
        $this->assertTrue($returnValue);
        $wpdb = $this->invokeMethod($dbManager,'fetch_wpdb');

        $record_count = $wpdb->get_col('select count(*) from ' .$wpdb->prefix.'s3plc');
        $this->assertEquals(200,$record_count[0]);


        // cleanup
        $this->invokeMethod($dbManager,'destroy_table');
        $this->remove_data_directory();
        return;
    }

}