<?php
/**
 * Created by PhpStorm.
 * User: cal
 * Date: 5/11/15
 * Time: 6:53 PM
 */

require_once 'S3PLCBaseTest.php';

use S3PLC\File;
use S3PLC\Options;
use S3PLC\Bootstrap;
use S3PLC\Db;
use S3PLC\LogCruncher;

class S3PLCTestBootstrap extends S3PLCBaseTest
{

    public function test_shortcode_top_episodes() {
        // create 5 sets of test files for 5 different MP3 across 5 days
        $this->create_data_directory();
        $this->generate_test_data(10,0,$changes=['key'=>'my_test_file-10.mp3']);
        $this->generate_test_data(20,0,$changes=['key'=>'my_test_file-20.mp3']);
        $this->generate_test_data(5,0, $changes=['key'=>'my_test_file-5.mp3']);
        $this->generate_test_data(14,0,$changes=['key'=>'my_test_file-14.mp3']);
        $this->generate_test_data(6,0, $changes=['key'=>'my_test_file-6.mp3']);

        // create posts
        $post = array('post_status' => 'publish', 'post_title' => 'Audio:' . rand_str(), 'post_type' => 'post', 'post_content' => rand_str());
        // create the post
        $post_id = $this->factory->post->create($post);
        $media = "http://elephpant.s3.amazonaws.com/my_test_file-10.mp3.mp3" .
            "\n39770015" .
            "\naudio/mpeg" .
            "\n" . 'a:3:{s:8:"duration";s:8:"00:16:30";s:8:"subtitle";s:35:"Interview with Michelangelo van Dam";s:6:"author";s:23:"Voices of the ElePHPant";}';
        update_post_meta($post_id, 'enclosure', $media);

        $post_id = $this->factory->post->create($post);
        $media = "http://elephpant.s3.amazonaws.com/my_test_file-20.mp3.mp3" .
            "\n39770015" .
            "\naudio/mpeg" .
            "\n" . 'a:3:{s:8:"duration";s:8:"00:16:30";s:8:"subtitle";s:35:"Interview with Michelangelo van Dam";s:6:"author";s:23:"Voices of the ElePHPant";}';
        update_post_meta($post_id, 'enclosure', $media);

        $post_id = $this->factory->post->create($post);
        $media = "http://elephpant.s3.amazonaws.com/my_test_file-5.mp3.mp3" .
            "\n39770015" .
            "\naudio/mpeg" .
            "\n" . 'a:3:{s:8:"duration";s:8:"00:16:30";s:8:"subtitle";s:35:"Interview with Michelangelo van Dam";s:6:"author";s:23:"Voices of the ElePHPant";}';
        update_post_meta($post_id, 'enclosure', $media);

        $post_id = $this->factory->post->create($post);
        $media = "http://elephpant.s3.amazonaws.com/my_test_file-14.mp3.mp3" .
            "\n39770015" .
            "\naudio/mpeg" .
            "\n" . 'a:3:{s:8:"duration";s:8:"00:16:30";s:8:"subtitle";s:35:"Interview with Michelangelo van Dam";s:6:"author";s:23:"Voices of the ElePHPant";}';
        update_post_meta($post_id, 'enclosure', $media);

        $post_id = $this->factory->post->create($post);
        $media = "http://elephpant.s3.amazonaws.com/my_test_file-6.mp3.mp3" .
            "\n39770015" .
            "\naudio/mpeg" .
            "\n" . 'a:3:{s:8:"duration";s:8:"00:16:30";s:8:"subtitle";s:35:"Interview with Michelangelo van Dam";s:6:"author";s:23:"Voices of the ElePHPant";}';
        update_post_meta($post_id, 'enclosure', $media);

        $dbManager       = new DB();
        $optionsManager  = new Options();
        $filesManager    = new File();
        $bootstrap       = new Bootstrap(plugin_dir_path( __FILE__ ).'../');
        $log_cruncher    = new LogCruncher($optionsManager,$filesManager,$dbManager,$bootstrap);

        // Read in all the test data into the DB
        $dbManager      = new DB();
        $optionsManager = new Options();
        $filesManager   = new File();

        $options = $optionsManager->get_options();
        $options['log_bucket']    = __DIR__;
        $options['process_count'] = 0;
        $options['log_dir']       = $this->generated_data_dir;
        $wpdb = $this->invokeMethod($dbManager,'fetch_wpdb');
        $this->invokeMethod($dbManager,'create_table');

        $returnValue = $log_cruncher->main($options);

        $this->assertEquals(1,$returnValue);

        $returnValue = $this->invokeMethod($bootstrap,'shortcode_top_episodes',[]);
        $this->assertNotEmpty($returnValue);

        $pattern = '/s3plc_downloads_count/sU';
        preg_match_all($pattern,$returnValue, $matches);
        $this->assertCount(5,$matches[0]);

        $pattern = '/<td class="s3plc_downloads_count">(.*)<\/td>/sU';
        preg_match_all($pattern,$returnValue, $matches);
        $this->assertEquals(20,$matches[1][0]);
        $this->assertEquals(14,$matches[1][1]);
        $this->assertEquals(10,$matches[1][2]);
        $this->assertEquals(6,$matches[1][3]);
        $this->assertEquals(5,$matches[1][4]);

        $this->invokeMethod($dbManager,'destroy_table');
        $this->remove_data_directory();

        return;
    }

    public function test_add_action_links() {
        $bootstrap       = new Bootstrap(plugin_dir_path( __FILE__ ).'../');
        $returnValue = $bootstrap->add_action_links([]);
        $this->assertCount(1,$returnValue);
        return;
    }

}