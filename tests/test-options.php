<?php
/**
 * Created by PhpStorm.
 * User: cal
 * Date: 5/9/15
 * Time: 11:29 AM
 */
require_once 'S3PLCBaseTest.php';

use S3PLC\Options;

class S3PLCTestOptions extends S3PLCBaseTest {

    public function test_options() {
        // test create
        $optionManager = new Options();
        $options = $optionManager->get_options();
        $this->assertCount(9,$options);

        // test Save
        $options['aws_access_key_id'] = 'FAKE';
        $optionManager->set_options($options);
        unset($options);
        $options = $optionManager->get_options();
        $this->assertEquals($options['aws_access_key_id'],'FAKE');

        // test extensions
        $options['extensions_to_count'] = '.mp3';
        $optionManager->set_options($options);

        unset($options);
        $options = $optionManager->get_options();
        $holding = explode(',',$options['extensions_to_count']);
        $this->assertCount(1,$holding);

        // test multiple extensions
        $options['extensions_to_count'] = '.mp3,.mov';
        $optionManager->set_options($options);

        unset($options);
        $options = $optionManager->get_options();
        $holding = explode(',',$options['extensions_to_count']);
        $this->assertCount(2,$holding);

        // test force
        $options = $optionManager->get_options(true);
        $this->assertNotEquals($options['aws_access_key_id'],'FAKE');

        // test remove
        $returnValue = $optionManager->remove_options();
        $this->assertTrue($returnValue);
        return;
    }
}