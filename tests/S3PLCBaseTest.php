<?php
/**
 * Created by PhpStorm.
 * User: cal
 * Date: 5/7/15
 * Time: 9:31 AM
 */
use S3PLC\File;
use S3PLC\Options;
use League\Flysystem\Adapter\Local;

abstract class S3PLCBaseTest extends WP_UnitTestCase {

    public $generated_data_dir = '/generated_data/';
    public $current_dir = __DIR__;

    public function setUp() {
        parent::setUp();
        global $wpdb;
        $sql = "drop table if exists " . $wpdb->prefix . "s3plc";
        $wpdb->query($sql);
        $this->remove_data_directory();

        remove_filter( 'query', array( $this, '_create_temporary_tables' ) );
        remove_filter( 'query', array( $this, '_drop_temporary_tables' ) );

    }

    public function tearDown() {
        global $wpdb;
        $sql = "drop table if exists " . $wpdb->prefix . "s3plc";
        $wpdb->query($sql);
        $this->remove_data_directory();
        parent::tearDown();
    }


    /*
     * Helper Functions
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    protected function flatten_tables_list($wpdb) {
        $return = $wpdb->get_results('show tables;');

        $dbs = [];
        foreach($return as $key=>$value) {
            $dbs[] = $return[$key]->Tables_in_wordpress_test;
        }
        return $dbs;
    }

    protected function generate_test_data($records_requested,$days=0,$changes=[]) {
        $files = [];

        for($lcvA=0;$lcvA<$records_requested;$lcvA++) {
            $payload = $this->generate_test_data_record($days,$changes);
            $files[] = 'x_'.md5(time()+rand(1,100000000));
            file_put_contents($this->current_dir . $this->generated_data_dir . $files[count($files)-1],$payload);
        }

        return $files;

    }

    protected function generate_multi_line_test_file($days=0,$changes=[]) {

        $payload = '';

        foreach ($changes as $thisChanges) {
            $payload .= $this->generate_test_data_record($days,$thisChanges) . "\n";
        }
        $file_name = 'x_'.md5(time()+rand(1,100000000));

        file_put_contents($this->current_dir. $this->generated_data_dir . $file_name ,$payload);
        return $file_name;
    }

    protected function generate_test_data_record($days=-1,$changes=[]) {
        /*
         78baff5e7664049aebab9866616fc4bedc15a90bf722ec8269e4d1c19c5ba7d2 elephpant [22/Apr/2015:21:12:43 +0000] 54.90.134.248 - F06D69794753EC38 REST.GET.OBJECT vote_039.mp3 "GET /vote_039.mp3 HTTP/1.1" 200 - 132344 35869861 71 68 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9 rv:28.0) Gecko/20100101 Firefox/28.0 (FlipboardProxy/1.1; +http://flipboard.com/browserproxy)" -


         owner      = 78baff5e7664049aebab9866616fc4bedc15a90bf722ec8269e4d1c19c5ba7d2
         bucket     = elephpant
         time       = [22/Apr/2015:21:12:43 +0000]
         ip         = 54.90.134.248
         requester  = -
         reqid      = F06D69794753EC38
         operation  = REST.GET.OBJECT
         key        = vote_039.mp3
         request    = "GET /vote_039.mp3 HTTP/1.1"
         status     = 200
         error      = -
         bytes      = 132344
         size       = 35869861
         total time = 71
         tunraround = 68
         referrer   = "-"
         useragent  = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9 rv:28.0) Gecko/20100101 Firefox/28.0 (FlipboardProxy/1.1; +http://flipboard.com/browserproxy)"
         version    = -
         iTunes/12.1.2 (Macintosh; OS X 10.10.2) AppleWebKit/0600.3.18"

         @todo need to be able to generate random file names if requested.
         */

        $record = [];
        $record['owner']      = '78baff5e7664049aebab9866616fc4bedc15a90bf722ec8269e4d1c19c5ba7d2';
        $record['bucket']     = 'elephpant';
        $record['time']       = date('[d/M/Y:H:i:s O]',$this->generate_timestamp($days));
        $record['ip']         = '54.90.134.248';
        $record['requester']  = '-';
        $record['reqid']      = 'F06D69794753EC38';
        $record['operation']  = 'REST.GET.OBJECT';
        $record['key']        = 'vote_039.mp3';
        $record['request']    = '"GET /vote_039.mp3 HTTP/1.1"';
        $record['status']     = 200;
        $record['error']      = '-';
        $record['bytes']      = 132344;
        $record['size']       = 35869861;
        $record['total time'] = 71;
        $record['tunraround'] = 68;
        $record['referrer']   = '"-"';
        $record['useragent']  = '"iTunes/12.1.2 (Macintosh; OS X 10.10.2) AppleWebKit/0600.3.18"';
        $record['version']    = '-';

        foreach ($changes as $key => $value) {
            $record[$key] = $value;
        }

        $payload = implode(" ",$record);

        return $payload;
    }

    protected function generate_timestamp($days=-30) {
        $dt = new DateTimeImmutable();
        if ($days<0) {

            $returnValue = $dt->sub(new DateInterval('P' . abs($days) . 'D'))->format('U');
        } else {
            $returnValue = $dt->add(new DateInterval('P' . $days . 'D'))->format('U');
        }
        return $returnValue;

    }

    protected function create_data_directory() {

        $adapter = new Local($this->current_dir);
        $fileManager  = new S3PLC\File();
        $flysystem = $fileManager->get_flysystem([],$adapter);

        if ($flysystem->has($this->generated_data_dir)) {
            $this->remove_data_directory();
        }


        $flysystem->createDir($this->generated_data_dir);
        return;
    }

    protected function remove_data_directory() {
        $adapter = new Local($this->current_dir);
        $fileManager  = new S3PLC\File();
        $flysystem = $fileManager->get_flysystem([],$adapter);
        $flysystem->deleteDir($this->generated_data_dir);

        return;
    }

}
