<?php
/**
 * Created by PhpStorm.
 * User: cal
 * Date: 5/9/15
 * Time: 11:53 AM
 */
require_once 'S3PLCBaseTest.php';

use S3PLC\File;
use S3PLC\Options;
use S3PLC\Db;

class S3PLCTestFiles extends S3PLCBaseTest
{

    public function test_is_connected() {
        $this->create_data_directory();

        // test whether we have a working local flysystem adapter
        $fileManager   = new File();
        $optionManager = new Options();
        $options = $optionManager->get_options();
        $options['log_bucket']    = __DIR__;
        $options['log_dir']       = $this->generated_data_dir;
        $options['process_count'] = 10;
        $flysystem = $fileManager->get_flysystem($options);
        $this->generate_test_data(5);
        $files = $flysystem->listContents($options['log_dir']);
        $this->assertCount(5,$files);
        $this->remove_data_directory();
        return;
    }

    public function test_fetch_log_files_list() {
        $this->create_data_directory();
        $this->generate_test_data(20);
        $fileManager = new File();
        $optionManager = new Options();
        $options = $optionManager->get_options();
        $options['log_bucket']    = __DIR__;
        $options['log_dir']       = $this->generated_data_dir;
        $options['process_count'] = 10;
        $fileManager->get_flysystem($options);
        $files = $this->invokeMethod($fileManager,'fetch_log_files_list',[$options]);
        $this->assertCount(10,$files);
        $this->remove_data_directory();
        return;
    }

    public function test_process_single_file() {
        $this->create_data_directory();
        $this->generate_test_data(20);
        $fileManager = new File();
        $optionManager = new Options();
        $options = $optionManager->get_options();
        $options['log_bucket']    = __DIR__;
        $options['process_count'] = 10;
        $options['log_dir']       = $this->generated_data_dir;
        $fileManager->get_flysystem($options);

        $files = $this->invokeMethod($fileManager,'fetch_log_files_list',[$options]);
        $matches = $this->invokeMethod($fileManager,'process_single_file',[$options['log_dir'].$files[0]['basename']]);

        $this->assertCount(1,$matches);
        $this->assertCount(18,$matches[0]);
        $this->remove_data_directory();
        return;
    }

    public function test_process_multipart_file() {
        $this->create_data_directory();
        $changes = [];
        $changes[] = [];
        $changes[] = ['operation'=>'REST.GET.HEAD'];
        $changes[] = ['status'=>206];
        $changes[] = [];
        $file = $this->generate_multi_line_test_file(0,$changes);
        $fileManager = new File();
        $optionManager = new Options();
        $options = $optionManager->get_options();
        $options['log_bucket']    = __DIR__;
        $options['process_count'] = 10;
        $options['log_dir']       = $this->generated_data_dir;
        $fileManager->get_flysystem($options);
        $matches = $this->invokeMethod($fileManager,'process_single_file',[$options['log_dir'].$file]);
        $this->assertCount(4,$matches);
        return;
    }

    public function test_apply_filters() {
        $this->create_data_directory();
        $this->generate_test_data(10);

        $fileManager = new File();
        $optionManager = new Options();
        $options = $optionManager->get_options();
        $options['log_bucket']    = __DIR__;
        $options['process_count'] = 10;
        $options['log_dir']       = $this->generated_data_dir;
        $fileManager->get_flysystem($options);

        $files = $this->invokeMethod($fileManager,'fetch_log_files_list',[$options]);
        $matches = $this->invokeMethod($fileManager,'process_single_file',[$options['log_dir'].$files[0]['basename']]);
        foreach($matches as $this_match) {
            $this_match = $this->invokeMethod($fileManager,'apply_filters',[$this_match,$options]);
            $this->assertNotEmpty($this_match);
        }
        // now let's try with a different extension

        $options['extensions_to_count'] = '.mov';
        $matches = $this->invokeMethod($fileManager,'apply_filters',[$matches,$options]);
        $this->assertEmpty($matches);
        $this->remove_data_directory();
        return;
    }

    public function test_filter_unneeded_records() {
        $fileManager = new File();
        $dbManager   = new DB();

        // test for passing in an empty array.
        $results = $this->invokeMethod($fileManager,'filter_unneeded_records',[[]]);
        $this->assertEmpty($results);

        // most common path
        $record = $this->invokeMethod($dbManager,'init_record');
        $record['operation'] = 'REST.GET.OBJECT';
        $record['status']    = 200;
        $results = $this->invokeMethod($fileManager,'filter_unneeded_records',[$record]);
        $this->assertNotEmpty($results);

        // 206 should still pass
        $record['status']=206;
        $results = $this->invokeMethod($fileManager,'filter_unneeded_records',[$record]);
        $this->assertNotEmpty($results);

        // 301 should not pass
        $record['status']=301;
        $results = $this->invokeMethod($fileManager,'filter_unneeded_records',[$record]);
        $this->assertNotEmpty($results);


        // GET.HEAD should not pass
        $record['operation'] = 'REST.GET.HEAD';
        $record['status']    = 200;
        $results = $this->invokeMethod($fileManager,'filter_unneeded_records',[$record]);
        $this->assertEmpty($results);
        return;
    }

    public function test_process_log_files() {
        $this->create_data_directory();
        $this->generate_test_data(10,0,$changes=['key'=>'my_test_file.mp3']);

        // create the table
        $dbManager      = new DB();
        $optionsManager = new Options();
        $filesManager   = new File();

        $options = $optionsManager->get_options();
        $options['log_bucket']    = __DIR__;
        $options['process_count'] = 10;
        $options['log_dir']       = $this->generated_data_dir;
        $wpdb = $this->invokeMethod($dbManager,'fetch_wpdb');
        $this->invokeMethod($dbManager,'create_table');


        $filesManager->get_flysystem($options);

        $files = $this->invokeMethod($filesManager,'fetch_log_files_list',[$options]);
        $this->assertCount(10,$files);

        $returnValue = $this->invokeMethod($filesManager,'process_log_files',[$files,$options]);

        $this->assertEquals($options['process_count'],$returnValue['my_test_file.mp3']);

        for($lcvA=1;$lcvA<=10;$lcvA++) {
            $retrieved_record = $this->invokeMethod($dbManager,'retrieve_record',[$lcvA]);
            $this->assertEquals($lcvA,$retrieved_record['ID']);
        }

        $files = $filesManager->get_flysystem()->listContents($this->generated_data_dir);
        $this->assertEmpty($files);


        $this->invokeMethod($dbManager,'destroy_table');
        $this->remove_data_directory($this->generated_data_dir);
        return;
    }


}