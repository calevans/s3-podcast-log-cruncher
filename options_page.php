<?php
/**
 * Admin options page for S3PLC
 *
 * @category Plugin
 * @package  S3PLC
 * @author   Cal Evans <cal@calevans.com>
 * @license  MIT
 * @link     https://opensource.org/licenses/MIT
 */

?>
<form method="post" action="options-general.php?page=<?php echo esc_attr( $this->determine_plugin_location() ); ?>">
<h2><?php esc_html( 'S3 Podcast Log Cruncher' ); ?></h2>
<label for="twitter_account">AWS Access ID : </label>
<input type="text" name="aws_access_key_id" id="aws_access_key_id" size="30" value="<?php esc_attr( $options['aws_access_key_id'] ); ?>" /> <br />

<label for="twitter_account">AWS Secret Access Key : </label>
<input type="text" name="aws_secret_access_key" id="aws_secret_access_key" size="30" value="<?php eesc_attr( $options['aws_secret_access_key'] ); ?>" /> <br />

<label for="twitter_account">AWS Log Bucket : </label>
<input type="text" name="log_bucket" id="log_bucket" size="30" value="<?php esc_attr( $options['log_bucket'] ); ?>" /> <br />

<label for="twitter_account">AWS Log Directory : </label>
<input type="text" name="log_dir" id="log_dir" size="30" value="<?php esc_attr( $options['log_dir'] ); ?>" /> <br />

<!-- Add a TEST button -->

<label for="twitter_account">Number of files to process in one run<br />(0 for all available) : </label>
<input type="text" name="process_count" id="process_count" size="6" value="<?php esc_attr( $options['process_count'] ); ?>" /> <br />

<label for="twitter_account">Which cron do you want to use? : </label>
<input type="text" name="cron_to_use" id="cron_to_use" size="30" value="<?php esc_attr( $options['cron_to_use'] ); ?>" /> <br />

<label for="twitter_account">Seconds Between Runs : </label>
<input type="text" name="seconds_between_runs" id="seconds_between_runs" size="30" value="<?php esc_attr( $options['seconds_between_runs'] ); ?>" /> <br />

<label for="twitter_account">Extensions to count : <br />(Comma delimeted list. e.g. '.mp3, .mp4') </label>
<TEXTAREA name="extensions_to_count" id="extensions_to_count" ><?php esc_attr( $options['extensions_to_count'] ); ?></TEXTAREA>
<br />
<label for="twitter_account">Days to keep history<br />(0 for never archive) : </label>
<input type="text" name="days_to_keep_history" id="days_to_keep_history" size="6" value="<?php esc_attr( $options['days_to_keep_history'] ); ?>" /> <br />

<p class="submit">
<input id="save-changes" name="save-changes" type="submit" class="button-primary" value="<?php esc_attr( 'Save Changes' ); ?>" />
</p>

<p class="submit">
<input id="run-imediate" name="run-immediate" type="submit" class="button-primary" value="<?php esc_attr( 'Run Immediately' ); ?>" />
</p>
</form>
