<?php
/**
 * File Manager for S3PLC
 *
 * @category Plugin
 * @package  S3PLC
 * @author   Cal Evans <cal@calevans.com>
 * @license  MIT
 * @link     https://opensource.org/licenses/MIT
 */

namespace S3PLC;

use Aws\S3\S3Client;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;
use League\Flysystem\AwsS3v2\AwsS3Adapter;

/**
 * Primary File manager for S3PLC. handles connecting to S3 and pulling in the log files.
 */
class File {

	/**
	 * The instantiated adapter
	 *
	 * @var object
	 */
	protected $flysystem;

	/**
	 * See comments below. This is dumb.
	 *
	 * @param array  $options The options we are operating under.
	 * @param object $adapter The adapter to use.
	 * @return object
	 * @todo refactor.
	 */
	public function get_flysystem( array $options = [], object $adapter = null ) {
		/*
		 * Yes, I know this is bad. However, I need lazy loading. I don't want
		 * the flysystem created until we actually need it to prevent
		 * unnecessary connections to S3.
		 *
		 * Move this to a wrapper class that will not connect until the first
		 * request. Then instantiate outside the class and inject.
		 */
		if ( is_null( $this->flysystem ) ) {
			$adapter         = $this->get_adapter( $options, $adapter );
			$this->flysystem = new Filesystem( $adapter );
		}

		return $this->flysystem;
	}

	/**
	 * If no Flysystem adapter was passed in, create the default S3.
	 *
	 * @param array  $options The options we are operating under.
	 * @param object $adapter The adapter to use.
	 * @return object
	 * @todo Refactor this. It's a mess. The name makes no sense because we can pass in an adapter.
	 */
	protected function get_adapter( array $options, object $adapter = null ) {
		if ( is_null( $adapter ) ) {
			// no adapter was passed in.
			if ( empty( $options['aws_access_key_id'] ) ) {
				// create a local adapter.
				$adapter = new Local( $options['log_bucket'] );
			} else {
				// create an S3 adapter.
				$client = S3Client::factory(
					array(
						'key'    => $options['aws_access_key_id'],
						'secret' => $options['aws_secret_access_key'],
						'region' => '',
					)
				);

				$adapter = new AwsS3Adapter( $client, $options['log_bucket'] );
			}
		}
		return $adapter;
	}

	/**
	 * Retrive the list of log files from AWS
	 *
	 * @param array $options The options we are operating under.
	 * @return array
	 *
	 * @todo options is not optional here so it should not contain = [].
	 */
	public function fetch_log_files_list( array $options = [] ): array {
		$return_value = [];
		// parameratize this.
		$files = $this->flysystem->listContents( $options['log_dir'] );

		foreach ( $files as $key => $value ) {
			if ( 'readme.txt' === $value['basename'] ) {
				continue;
			}

			$return_value[] = $value;
		}

		if ( $options['process_count'] > 0 ) {
			$return_value = array_slice( $return_value, 0, $options['process_count'] );
		}

		return $return_value;
	}

	/**
	 * Process the list of log file entries
	 *
	 * @param array $log_files The lsit of log files to process.
	 * @param array $options The options we are operating under.
	 * @return array
	 */
	public function process_log_files( array $log_files, array $options ): array {
		$media_files_touched = [];
		$db_manager          = new DB(); // inject this.

		foreach ( $log_files as $this_log_file ) {
			$matches = $this->process_single_file( $options['log_dir'] . $this_log_file['basename'] );

			foreach ( $matches as $this_match ) {
				$this_match = $this->filter_unneeded_records( $this_match );
				$this_match = $this->apply_filters( $this_match, $options );

				if ( ! empty( $this_match ) ) {
					$this_match['post_id'] = $db_manager->find_post_by_media( $this_match['key'] );
					$db_manager->store_record( $this_match );

					if ( ! isset( $media_files_touched[ $this_match['key'] ] ) ) {
						$media_files_touched[ $this_match['key'] ] = 0;
					}

					$media_files_touched[ $this_match['key'] ] += 1;
				}
			}
		}

		return $media_files_touched;
	}

	/**
	 * Filter out any log entries we are not interested in.
	 *
	 * @param array $matches The record to process.
	 * @return array
	 * @todo refactor to one big if OR statment.
	 */
	protected function filter_unneeded_records( array $matches ): array {
		$return_value = $matches;
		if ( empty( $matches ) ) {
			$return_value = [];
		} elseif ( 'REST.GET.OBJECT' !== $matches['operation'] ) {
			$return_value = [];
		} elseif ( ( (int) $matches['status'] <= 200 ) && ( (int) $matches['status'] > 300 ) ) {
			$return_value = [];
		}

		return $return_value;
	}

	/**
	 * Read a single log file and process it's contents.
	 *
	 * @param string $file_name The name of the log file.
	 * @return array
	 */
	protected function process_single_file( $file_name ) {
		$file_contents       = $this->flysystem->readAndDelete( $file_name );
		$pattern             = '/(?P<owner>\S+) (?P<bucket>\S+) (?P<time>\[[^]]*\]) (?P<ip>\S+) (?P<requester>\S+) (?P<reqid>\S+) (?P<operation>\S+) (?P<key>\S+) (?P<request>"[^"]*") (?P<status>\S+) (?P<error>\S+) (?P<bytes>\S+) (?P<size>\S+) (?P<totaltime>\S+) (?P<turnaround>\S+) (?P<referrer>"[^"]*") (?P<useragent>"[^"]*") (?P<version>\S)/';
		$file_contents_array = explode( "\n", $file_contents );
		$matches             = [];

		foreach ( $file_contents_array as $data ) {
			preg_match( $pattern, $data, $matches[] );
		}

		$return_value = [];

		foreach ( $matches as $this_match ) {
			$holding = $this->remove_integer_records( $this_match );

			if ( empty( $holding ) ) {
				continue;
			}

			$holding['time'] = strtotime(
				strtr(
					$holding['time'],
					[
						'[' => '',
						']' => '',
					]
				)
			);
			$return_value[]  = $holding;
		}

		return $return_value;
	}

	/**
	 * Remove integer records and only leave an associative array
	 *
	 * @param array $matches The array to process.
	 * @return array
	 */
	protected function remove_integer_records( $matches = [] ): array {
		$return_value = [];

		foreach ( $matches as $key => $value ) {
			if ( (int) $key === $key ) {
				continue;
			}

			$return_value[ $key ] = $value;
		}

		return $return_value;
	}

	/**
	 * Apply file extension filters to the list of matches.
	 *
	 * @param array $matches The matches to filter.
	 * @param array $options The options array.
	 * @return array
	 */
	protected function apply_filters( array $matches = [], array $options ): array {
		if ( empty( $matches ) || ! isset( $matches['key'] ) ) {
			return [];
		}

		$return_value = [];
		$extensions   = explode( ',', $options['extensions_to_count'] );

		foreach ( $extensions as $this_extension ) {
			if ( strpos( $matches['key'], $this_extension ) !== false ) {
				$return_value = $matches;
				break;
			}
		}
		return $return_value;
	}

}
