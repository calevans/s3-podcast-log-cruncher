<?php
/**
 * Options Manager
 *
 * @category Plugin
 * @package  S3PLC
 * @author   Cal Evans <cal@calevans.com>
 * @license  MIT
 * @link     https://opensource.org/licenses/MIT
 */

namespace S3PLC;

/**
 * Manage the options for the plugin
 */
class Options {

	/**
	 * The database key to store the options under.
	 *
	 * @var string
	 */
	public $option_key = 's3plc_plugin_options';

	/**
	 * The current options array.
	 *
	 * @var array
	 */
	protected $optons = [];

	/**
	 * Unused
	 *
	 * @var string
	 */
	protected $hash = null;

	/**
	 * Retrieve the options array form the database
	 *
	 * @param bool $force If true, return a blank array.
	 * @return array
	 * @todo Add code here to hash the array. Add code on write to only write if
	 *       hash is different.
	 * @todo refactor to reduce complexity
	 * */
	public function get_options( bool $force = false ): array {
		if ( $force ) {
			$return_value = $this->init_option();
		} elseif ( ! empty( $this->options ) ) {
			$return_value = $this->options;
		} else {
			$return_value = get_option( $this->option_key );
		}

		if ( empty( $return_value ) ) {
			$return_value = $this->init_option();
		}

		$return_value['cron_to_use'] = trim( $return_value['cron_to_use'] );
		return $return_value;
	}

	/**
	 * Filters and then writes the options array to the database
	 *
	 * @param array $options A properly formatted options array.
	 */
	public function set_options( array $options ) {
		$template = $this->get_options();

		$template['aws_access_key_id']     = filter_var( $options['aws_access_key_id'], FILTER_SANITIZE_STRING );
		$template['aws_secret_access_key'] = filter_var( $options['aws_secret_access_key'], FILTER_SANITIZE_STRING );
		$template['log_bucket']            = filter_var( $options['log_bucket'], FILTER_SANITIZE_STRING );
		$template['log_dir']               = filter_var( $options['log_dir'], FILTER_SANITIZE_STRING );
		$template['process_count']         = filter_var( $options['process_count'], FILTER_SANITIZE_STRING );
		$template['seconds_between_runs']  = filter_var( $options['seconds_between_runs'], FILTER_SANITIZE_STRING );
		$template['cron_to_use']           = filter_var( $options['cron_to_use'], FILTER_SANITIZE_STRING );
		$template['days_to_keep_history']  = filter_var( $options['days_to_keep_history'], FILTER_SANITIZE_STRING );

		$template['extensions_to_count'] = filter_var( $options['extensions_to_count'], FILTER_SANITIZE_STRING );

		$template['process_count']        = filter_var( $options['process_count'], FILTER_SANITIZE_NUMBER_INT );
		$template['seconds_between_runs'] = filter_var( $options['seconds_between_runs'], FILTER_SANITIZE_NUMBER_INT );
		$template['days_to_keep_history'] = filter_var( $options['days_to_keep_history'], FILTER_SANITIZE_NUMBER_INT );

		if ( substr( $template['log_dir'], -1 ) !== DIRECTORY_SEPARATOR ) {
			$template['log_dir'] = trim( $template['log_dir'] ) . DIRECTORY_SEPARATOR;
		}

		update_option( $this->option_key, $template );
	}

	/**
	 * Removes the array key from the database
	 *
	 * @return bool
	 */
	public function remove_options(): bool {
		return delete_option( $this->option_key );
	}

	/**
	 * Creates an empty array of options
	 *
	 * @return array
	 */
	public function init_option(): array {
		return [
			'aws_access_key_id'     => '',
			'aws_secret_access_key' => '',
			'log_bucket'            => '',
			'log_dir'               => '',
			'process_count'         => 0,
			'seconds_between_runs'  => 43200,
			'cron_to_use'           => 'WordPress',
			'extensions_to_count'   => '.mp3',
			'days_to_keep_history'  => 0,
		];
	}

}
