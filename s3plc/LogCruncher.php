<?php
/**
 * Main processing logic for S3PLC
 *
 * @category Plugin
 * @package  S3PLC
 * @author   Cal Evans <cal@calevans.com>
 * @license  MIT
 * @link     https://opensource.org/licenses/MIT
 */

namespace S3PLC;

use S3PLC\Options;
use S3PLC\File;
use S3PLC\Bootstrap;
use S3PLC\DB;

/**
 * Main methods of the system. Processes the S3 log files.
 *
 * @todo Add a method to add the # of downloads to the end of every post that has a media file.
 * @todo Add feature to create a filter on referrer
 * @todo implement caching for the main queries.
 * @todo change options page type of cron to a select box
 * @todo register deactivate and cancel any cron jobs
 * @todo register delete and remove tables and data
 * @todo add install sanity check (php 5.5)
 * @todo "run immediately" should popup a modal window and show progress.
 * @todo Keep all the WP stuff in this class. Migrate everything else into it's own class or classes.
 */
class LogCruncher {

	/**
	 * Aray of post ids.
	 *
	 * @var array
	 */
	protected $post_ids = [];

	/**
	 * The current options being used.
	 *
	 * @var array
	 */
	protected $options;

	/**
	 * An instance of S3PLC\File
	 *
	 * @var File
	 */
	protected $file;

	/**
	 * An instance of S3PLC\DB
	 *
	 * @var DB
	 */
	protected $db;

	/**
	 * An instance of S3PLC\Bootstrap
	 *
	 * @var Bootstrap
	 */
	protected $bootstrap;

	/**
	 * Constructor
	 *
	 * @param Options   $options_manager Injected Options Manager.
	 * @param File      $file_manager Injected File Manager.
	 * @param DB        $db_manager Injected DB Manager.
	 * @param Bootstrap $bootstrap Injected Bootstrap.
	 * @param string    $plugin_dir The physical path to the plugin dir.
	 */
	public function __construct(
		Options $options_manager = null,
		File $file_manager = null,
		DB $db_manager = null,
		Bootstrap $bootstrap = null,
		string $plugin_dir = ''
	) {
		if ( empty( $plugin_dir ) ) {
			/**
			 * If a plugin_dir is not passed in, take the current directory and
			 *  back up one. This assumes that s3plc will always be one level
			 * deep from the root of the plugin. I wish WordPress has a method
			 * of determining the actual plugin root dir. (stupid WordPress)
			 *
			 * @todo is this right? plugin_dir_path should actually give you the path.
			 */
			$plugin_dir = realpath( plugin_dir_path( __FILE__ ) . '../' );
		}
		if ( is_null( $options_manager ) ) {
			$options_manager = new Options();
		}

		$this->options = $options_manager;

		if ( is_null( $file_manager ) ) {
			$file_manager = new File();
		}

		$this->file = $file_manager;

		if ( is_null( $db_manager ) ) {
			$db_manager = new DB();
		}

		$this->db = $db_manager;

		if ( is_null( $bootstrap ) ) {
			$bootstrap = new Bootstrap( $plugin_dir );
		}

		$this->bootstrap = $bootstrap;
	}

	/**
	 * Main method for crunching logs
	 *
	 * @param array  $options The options for this run.
	 * @param object $adapter The flysystem adapter to use.
	 * @return bool
	 */
	public function main( array $options = [], object $adapter = null ): bool {
		if ( empty( $options ) ) {
			$options = $this->options->get_options();
		}

		$this->file->get_flysystem( $options, $adapter );

		if ( ! $this->sanity_check( $options ) ) {
			return false;
		}

		$log_files           = $this->file->fetch_log_files_list( $options );
		$media_files_touched = $this->file->process_log_files( $log_files, $options );
		$this->update_posts( $media_files_touched );
		$this->rollup_history( $options );
		$this->bootstrap->schedule_next_event();

		return true;
	}

	/**
	 * Makes sure we have a table before we write.
	 *
	 * @param array $options The options for this run.
	 * @return bool
	 */
	protected function sanity_check( array $options = [] ): bool {
		// Check for the table in the DB plus anything else necessary for a run.
		// this is not the same as an install sanity check.
		$return_value = false;
		$return_value = $this->db->sanity_check();

		if ( $return_value ) {
			if ( empty( $options ) ) {
				$options = $this->options->get_options();
			}
			try {
				$this->file->get_flysystem( $options )->listContents( $options['log_dir'] );
				$return_value = true;
			} catch ( \Exception $e ) {
				print_r( $e );
				$return_value = false;
			}
		}
		return $return_value;
	}

	/**
	 * Converts individual records into summary records.
	 *
	 * @param array $options The options for this run.
	 * @return int
	 */
	protected function rollup_history( array $options ): int {
		if ( $options['days_to_keep_history'] <= 0 ) {
			return 0;
		}

		$rows    = $this->db->get_rows_to_rollup( $options );
		$holding = $this->db->remove_detail_records( $rows );
		$counter = $this->db->create_summary_records( $holding );

		return $counter;
	}


	/**
	 * Really not sure. Gotta figure this one out.
	 *
	 * @param array $media_files_touched Array of media files.
	 * @return int
	 */
	protected function update_posts( array $media_files_touched ): int {
		$counter = 0;
		foreach ( $media_files_touched as $key => $value ) {
			$this->db->update_single_post( $key );
			$counter++;
		}

		// this method will sum the downloads for each key in the table.
		// then it will write a summary record for that key/day/month/year
		// then it will update the post_meta with a _s3plc_downloads record with the total # of downloads, ever. create it if it doesn't exist, update it if it does.
		// If store_history is >0 then delete any records that are older than the prescribed number of days.
		return $counter;
	}

}
