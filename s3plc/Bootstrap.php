<?php
/**
 * Database connection for S3PLC
 *
 * @category Plugin
 * @package  S3PLC
 * @author   Cal Evans <cal@calevans.com>
 * @license  MIT
 * @link     https://opensource.org/licenses/MIT
 */

namespace S3PLC;

use S3PLC\DB;
use S3PLC\Options;

/**
 * Bootstrap the plugin
 *
 * @todo register_uninstall_hook($file, $callback)Remove the table, delete all data except downloads
 */
class Bootstrap {

	/**
	 * The directory this plugin is located in.
	 *
	 * @var string
	 */
	protected $plugin_dir;

	/**
	 * The database manager.
	 *
	 * @var DB
	 */
	protected $db_manager;

	/**
	 * Contructor
	 *
	 * @param string $plugin_dir the directory the plugin is in.
	 * @param DB     $db_manager The database manager.
	 */
	public function __construct(
		string $plugin_dir,
		DB $db_manager = null
	) {
		$this->plugin_dir = $plugin_dir;

		if ( is_null( $db_manager ) ) {
			$db = new DB(); // yes, I need to inject this.
		}

		$this->db_manager = $db;
	}

	/**
	 * Set everything up for WordPress
	 */
	public function init(): void {
		register_activation_hook( __FILE__, [ $this, 'activate' ] );
		register_deactivation_hook( __FILE__, [ $this, 'deactivate' ] );
		add_action( 'admin_menu', [ $this, 'admin_menu' ], 11 );
		add_filter( 'plugin_action_links_' . $this->determine_plugin_slug(), [ $this, 'add_action_links' ] );
		add_action( 's3plc_main', 's3_plc_cron_processor' );
		add_filter( 'the_content', [ $this, 'add_download_count_to_a_post' ] );
		add_shortcode( 's3plc_top_episodes', [ $this, 'shortcode_top_episodes' ] );
		/* add_action( 'wp_dashboard_setup', [$this,'add_dashboard_widget'] ); */
	}

	/**
	 * Called upon plugin activation
	 */
	public function activate(): void {
		$this->db->create_table();
		$this->schedule_next_event();
	}

	/**
	 * Called upon plugin deactivation
	 *
	 * @todo remove the table
	 */
	public function deactivate(): void {
		$this->cancel_wordpress_cron();
	}

	/**
	 * Add the admin menu
	 */
	public function admin_menu(): void {
		add_submenu_page( 'options-general.php', 'S3 Podcast Log Cruncher', 'S3 Podcast Log Cruncher', 'manage_options', $this->plugin_dir, [ $this, 'admin_options_page' ] );
	}

	/**
	 * Include the options page.
	 *
	 * @todo WHY???
	 */
	public function admin_options_page(): void {
		// need to inject the options object somehow.
		$options_manager = new Options();
		$options         = $options_manager->get_options();
		include $this->determine_plugin_location() . '/options_page.php';
	}

	/**
	 * Add the download count to a given post.
	 *
	 * @param string $content The content of the post.
	 * @todo refactor Move to a new class. Should not be part of Bootstrap.
	 */
	public function add_download_count_to_a_post( string $content ): string {
		if ( ! is_single() ) {
			return $content;
		}

		if ( empty( $content ) ) {
			return $content;
		}

		$id = get_the_ID();
		if ( empty( $id ) || (int) $id <= 0 ) {
			return $content;
		}

		$downloads = get_post_meta( $id, 's3plc_downloads', true );

		return $content
		  . '<div class="s3plc_container">'
		  . '<span class="s3plc_text">Downloads: </span><span class="s3plc_downloads">' . $downloads . '</span>'
		  . '</div>';
	}

	/**
	 * Process the shortcode for the top episodes.
	 *
	 * @param array $attrs Attributes.
	 * @return string
	 *
	 * @todo Add parameter to specify number of days so that it's not always
	 *       from the beginning of time.
	 * @todo refactor and put in a shortcode class. no need for this to be in
	 *       bootstrap
	 */
	public function shortcode_top_episodes( array $attrs = [] ): string {
		$wpdb       = $this->db_manager->fetch_wpdb();
		$attributes = shortcode_atts(
			[
				'episode_count' => 10,
				'desc_asc'      => 'DESC',
			],
			$attrs
		);

		$attributes['desc_asc'] = strtoupper( $attributes['desc_asc'] );

		$results = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT pm.post_id,
			      	  CAST(meta_value AS unsigned) as dload_count
				 	 FROM $wpdb->postmeta pm
				  WHERE pm.meta_key='s3plc_downloads'
				  ORDER BY dload_count %1s
					LIMIT 0, %2d;",
				[
					$attributes['desc_asc'],
					$attributes['episode_count'],
				]
			),
			ARRAY_A
		);

		$ids       = [];
		$post_list = [];

		foreach ( $results as $this_result ) {
			$ids[]                                = $this_result['post_id'];
			$post_list[ $this_result['post_id'] ] = $this_result['dload_count'];
		}

		// ids doesn't exist.
		$posts = get_posts(
			[
				'posts_per_page' => $attributes['episode_count'],
				'meta_key'       => 's3plc_downloads',
				'orderby'        => 'meta_value_num',
				'meta_query'     => [ 'key' => 's3plc_downloads' ],
			]
		);

		$payload  = '<table class="s3plc_top_episodes_table">';
		$payload .= '<tr><th>Downloads</th><th>Post Name</th></tr>' . "\n";

		foreach ( $posts as $this_post ) {
			$downloads = get_post_meta( $this_post->ID, 's3plc_downloads', true );
			$payload  .= '<tr class="s3plc_downloads_tr">' . "\n";
			$payload  .= '  <td class="s3plc_downloads_count">' . $downloads . '</td>' . "\n";
			$payload  .= '  <td class="s3plc_downloads_post_name"><a href="' . esc_url( get_permalink( $this_post->ID ) ) . '">' . $this_post->post_title . '</a></td>' . "\n";
			$payload  .= '</tr>' . "\n";
		}
		$payload .= '</table>' . "\n";
		return $payload;
	}

	/**
	 * Calculate and return the plugin slug. Used below in add_action_links
	 *
	 * @param string $type The tupe of plugin to return .
	 */
	public function determine_plugin_slug( string $type = 'full' ): string {
		if ( empty( $this->plugin_slug ) ) {
			$main_file         = realpath( pathinfo( __FILE__, PATHINFO_DIRNAME ) . '/../' ) . '/s3-podcast-log-cruncher.php';
			$pieces_parts      = explode( DIRECTORY_SEPARATOR, $main_file );
			$this->plugin_slug = $pieces_parts[ count( $pieces_parts ) - 2 ] . DIRECTORY_SEPARATOR . $pieces_parts[ count( $pieces_parts ) - 1 ];
		}

		if ( 'full' === $type ) {
			return $this->plugin_slug;
		}

		$pieces_parts = explode( DIRECTORY_SEPARATOR, $this->plugin_slug );

		return $pieces_parts[ count( $pieces_parts ) - 2 ];
	}

	/**
	 * Return the plugin directory
	 *
	 * @todo is this needed? If so, rename get_plugin_location
	 */
	public function determine_plugin_location(): string {
		return $this->plugin_dir;
	}

	/**
	 * Add the 'Settings' link to the plugin entry on the List Plugins page
	 *
	 * @param array $links array of links passed in.
	 */
	public function add_action_links( array $links ): array {
		$mylinks = [
			'<a href="' . admin_url( 'options-general.php?page=' . $this->determine_plugin_slug( 'partial' ) ) . '">Settings</a>',
		];
		return array_merge( $links, $mylinks );
	}

	/**
	 * Cancel the WordPress cron.
	 *
	 * @todo should not be part of bootstrap
	 */
	public function cancel_wordpress_cron(): void {
		$options = ( new Options() )->get_options();

		if ( 'WordPress' !== $options['cron_to_use'] ) {
			return;
		}

		wp_clear_scheduled_hook( 's3plc_main' );
	}

	/**
	 * Called after an event runs, it schedules the next event for x seconds
	 * into the future
	 *
	 * @param int $seconds Number of seconds into the future to schedule the next event.
	 * @todo should not be part of bootstrap
	 * @todo we only support WordPress. We need to support external crons.
	 */
	public function schedule_next_event( int $seconds = 0 ): void {
		$options = ( new Options() )->get_options();

		if ( 'WordPress' !== $options['cron_to_use'] ) {
			return;
		}

		$options['seconds_between_runs'] = $seconds > 0 ? $seconds : $options['seconds_between_runs'];

		wp_schedule_single_event( time() + $options['seconds_between_runs'], 's3plc_main' );
	}

	/**
	 * Handle the form returns from the admin menu
	 *
	 * @todo should not be part of bootstrap
	 * @todo nonce verification
	 */
	public function process_form(): void {
		$options_manager = new Options();
		// phpcs:ignore WordPress.Security.NonceVerification.Missing
		$options_manager->set_options( $_POST );

		if ( ! wp_next_scheduled( 's3plc_cron' ) ) {
			$this->schedule_next_event();
		}
	}

	/**
	 * Register the dashboard widget
	 */
	public function add_dashboard_widget(): void {
		wp_add_dashboard_widget(
			's3plc_dashboard_graph',              // Widget slug.
			'S3PLC Graph',                        // Title.
			[ $this, 'display_dashboard_widget' ] // Display function.
		);
	}

	/**
	 * Honestly, I have no idea. I think the widget stuff is still in testing
	 * and this is just a test. Not sure tho.
	 */
	public function display_dashboard_widget():void {
		echo 'Dashboard Widget!';
	}
}
