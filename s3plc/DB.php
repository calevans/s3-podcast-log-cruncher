<?php
/**
 * Database connection for S3PLC
 *
 * @category Plugin
 * @package  S3PLC
 * @author   Cal Evans <cal@calevans.com>
 * @license  MIT
 * @link     https://opensource.org/licenses/MIT
 */

namespace S3PLC;

/**
 * Manages the database connection and all access to the DB.
 */
class DB {

	/**
	 * The database version
	 *
	 * @var string
	 */
	public $version = '1.0.0';

	/**
	 * An array of post ids.
	 *
	 * @var array
	 */
	protected $post_ids = [];

	/**
	 * Make sure the table exists before we try to write to it.
	 *
	 * @return bool
	 */
	public function sanity_check(): bool {
		$wpdb   = $this->fetch_wpdb();
		$tables = $wpdb->get_results( 'show tables;' ); // db call ok.
		foreach ( $tables as $this_table ) {
			foreach ( $this_table as $key => $value ) {
				if ( $value === $wpdb->prefix . 's3plc' ) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Create the table
	 */
	public function create_table(): void {
		$wpdb               = $this->fetch_wpdb();
		$current_db_version = get_option( 's3plc_db_version' );

		$charset_collate = $wpdb->get_charset_collate();
		$sql             = 'CREATE TABLE ' . $wpdb->prefix . 's3plc (
            `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
            `post_id` bigint(20) unsigned,
            `owner` varchar(64) DEFAULT NULL,
            `bucket` varchar(128) DEFAULT NULL,
            `time` bigint(20) DEFAULT NULL,
            `ip`  varchar(128) DEFAULT NULL,
            `requester`  varchar(64) DEFAULT NULL,
            `reqid`  varchar(16) DEFAULT NULL,
            `operation` varchar(64) DEFAULT NULL,
            `key` varchar(254) DEFAULT NULL,
            `request` mediumtext,
            `status` int DEFAULT 0,
            `error` varchar(128) DEFAULT NULL,
            `bytes` bigint(20) DEFAULT NULL,
            `size` bigint(20) DEFAULT NULL,
            `totaltime` bigint(20) DEFAULT NULL,
            `turnaround` bigint(20) DEFAULT NULL,
            `referrer` mediumtext,
            `useragent` mediumtext,
            `version` varchar(64) DEFAULT NULL,
            PRIMARY KEY (ID),
            KEY `i_key` (`key`),
            KEY `i_totaltime` (`totaltime`) ) ' . $charset_collate . ';';

		include_once ABSPATH . 'wp-admin/includes/upgrade.php';
		dbDelta( $sql );
		update_option( 's3plc_db_version', $this->version );
	}

	/**
	 * Remove the table and all of it's data.
	 */
	public function destroy_table() {
		$wpdb = $this->fetch_wpdb();
		$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}s3plc" );
		delete_option( 's3plc_db_version' );
	}

	/**
	 * Create a blank record
	 *
	 * @return array
	 */
	public function init_record(): array {
		// move this into a table gateway?
		return [
			'ID'         => null,
			'post_id'    => null,
			'owner'      => null,
			'bucket'     => null,
			'time'       => null,
			'ip'         => null,
			'requester'  => null,
			'reqid'      => null,
			'operation'  => null,
			'key'        => null,
			'request'    => null,
			'status'     => null,
			'error'      => null,
			'bytes'      => null,
			'size'       => null,
			'totaltime'  => null,
			'turnaround' => null,
			'referrer'   => null,
			'useragent'  => null,
			'version'    => null,
		];
	}

	/**
	 * This is just so we don't have globals scattered all over the place.
	 *
	 * @todo this need to be protected but right now rollup history needs to
	 *       access the DB.
	 */
	public function fetch_wpdb() {
		global $wpdb;
		return $wpdb;
	}

	/**
	 * Fetch the record for a given id
	 *
	 * @param int $id the record to retrieve.
	 * @return array
	 */
	public function retrieve_record( int $id ): ?array {
		$wpdb         = $this->fetch_wpdb();
		$return_value = $wpdb->get_row(
			$wpdb->prepare(
				"select * from {$wpdb->prefix}s3plc where ID=%d",
				[ $id ]
			),
			ARRAY_A
		);
		return $return_value;
	}

	/**
	 * Locate a post by it's media file name.
	 *
	 * @param string $media_file the name of the media file.
	 * @return int
	 */
	public function find_post_by_media( string $media_file ): int {
		$return_value = 0;
		if ( ! array_key_exists( $media_file, $this->post_ids ) ) {
			$wpdb    = $this->fetch_wpdb();
			$post_id = $wpdb->get_col(
				$wpdb->prepare(
					"SELECT pm.post_id
             FROM {$wpdb->postmeta} pm
                  left join {$wpdb->posts} p on p.ID = pm.post_id
            WHERE p.post_status = 'publish'
                  and meta_key='enclosure'
		              and meta_value like %s",
					[ '%' . $wpdb->esc_like( $media_file ) . '%' ]
				)
			);

			if ( (int) $post_id > 0 ) {
				$this->post_ids[ $media_file ] = $post_id[0];
				$return_value                  = $post_id[0];
			}
		} else {
			$return_value = $this->post_ids[ $media_file ];
		}
		return $return_value;
	}

	/**
	 * Store a record in the database
	 *
	 * @param array $record the record to store.
	 * @return int
	 */
	public function store_record( array $record ): int {
		if ( empty( $record ) ) {
			return -1;
		}
		$wpdb   = $this->fetch_wpdb();
		$record = $this->remove_record_id( $record );
		if ( $wpdb->insert( $wpdb->prefix . 's3plc', $record ) ) {
			$new_record_id = $wpdb->insert_id;
		} else {
			$new_record_id = false;
		}
		return $new_record_id;
	}

	/**
	 * Remove the record id field.
	 *
	 * @param array $record The record.
	 * @return array
	 */
	protected function remove_record_id( array $record ): array {
		if ( isset( $record['ID'] ) ) {
			unset( $record['ID'] );
		}

		return $record;
	}

	/**
	 * Get all the records that can be summarized according to the options
	 *
	 * @param string $media_file the name of the media file to pull by.
	 */
	public function update_single_post( string $media_file ): void {
		$post_id = $this->find_post_by_media( $media_file );

		if ( ! $post_id ) {
			return;
		}

		$wpdb   = $this->fetch_wpdb();
		$count  = $wpdb->get_col(
			$wpdb->prepare(
				"SELECT count(*) as total
			   						 FROM {$wpdb->prefix}s3plc
										WHERE `key` = %s
													and operation<>'SUMMARY'",
				$media_file
			)
		)[0];
		$count += $wpdb->get_col(
			$wpdb->prepare(
				"SELECT sum(bytes) as total
				   FROM {$wpdb->prefix}s3plc
					WHERE `key` = %s
								and operation='SUMMARY'",
				$media_file
			)
		)[0];
		update_post_meta( $post_id, 's3plc_downloads', $count );
	}

	/**
	 * Create summary records for records being removed from history.
	 *
	 * @param array $holding List of records to summarize.
	 * @return int
	 */
	public function create_summary_records( $holding ): int {
		$counter = 0;
		$wpdb    = $this->fetch_wpdb();

		foreach ( $holding as $key => $value ) {
			$results = $wpdb->get_col(
				$wpdb->prepare(
					"SELECT ID
              FROM {$wpdb->prefix}s3plc
             WHERE operation='SUMMARY'
									 and request=%s",
					[ $key ]
				),
				ARRAY_A
			);

			if ( ! empty( $results ) ) {
				$record = $this->retrieve_record( $value['ID'] );
				$wpdb->delete( $wpdb->prefix . 's3plc', [ 'ID' => $value['ID'] ] );
			} else {
				$record              = $this->init_record();
				$record['operation'] = 'SUMMARY';
				$record['request']   = $key;
				$record['bytes']     = 0;
				$record['key']       = $value['key'];
			}

			$record['bytes'] += $value['count'];

			$wpdb->insert( $wpdb->prefix . 's3plc', $record );
			$counter++;
		}
		return $counter;
	}

	/**
	 * Remove all the detail rows passed in. Usually because they have been
	 * rolled up into history.
	 *
	 * @param array $rows the rows to be removed from detail.
	 * @return array
	 */
	public function remove_detail_records( array $rows ): array {
		$wpdb    = $this->fetch_wpdb();
		$holding = [];
		foreach ( $rows as $this_record ) {
			if ( ! array_key_exists( $this_record['sort_order'], $holding ) ) {
				$holding[ $this_record['sort_order'] ] = [
					'count' => 0,
					'key'   => $this_record['key'],
				];
			}

			$holding[ $this_record['sort_order'] ]['count'] += 1;
			$wpdb->delete( $wpdb->prefix . 's3plc', [ 'ID' => $this_record['ID'] ] );
		}

		return $holding;
	}

	/**
	 * Get all the records that can be summarized according to the options
	 *
	 * @param array $options Standard options array.
	 * @return array
	 */
	public function get_rows_to_rollup( array $options ): array {
		$wpdb = $this->fetch_wpdb();

		$results = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT *,
								CONCAT(`key`,'-',FROM_UNIXTIME(`time`,'%%Y-%%m-%%d')) as sort_order
					 FROM {$wpdb->prefix}s3plc
					WHERE (DATE_ADD(FROM_UNIXTIME(`time`,'%%Y-%%m-%%d'), INTERVAL %d DAY) > now())=1
					ORDER BY sort_order",
				$options['days_to_keep_history']
			),
			ARRAY_A
		);
		return $results;
	}

}
