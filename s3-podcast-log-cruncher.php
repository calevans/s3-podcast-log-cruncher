<?php
/**
 * Main entry point for S3PLC
 *
 * @category Plugin
 * @package  S3PLC
 * @author   Cal Evans <cal@calevans.com>
 * @license  MIT
 * @link     https://opensource.org/licenses/MIT
 */

/**
 * Plugin Name: S3 Podcast Log Cruncher
 * Version: 1.0
 * Description: Reads in log files from an S3 bucket, stores them in the db, and attempts to match them up to posts.
 * Author: Cal Evans
 * Author URI: http://blog.calevans.com
 * Plugin URI: http://blog.calevans.com
 * Text Domain: s3plc
 * Domain Path: /languages
 *
 * @todo abstract the location of the media file and build different classes for different podcasting plugins.
 * @todo add "Test Connection" button to the AWS credentials area.
 * @todo pretty up the options screen
 * @todo add a dashboard graph
 * @todo add an option for a widget overall downloads in last 30 days/this episode's downloads if is_single
 */

require 'vendor/autoload.php';

use S3PLC\Bootstrap;
use S3PLC\LogCruncher;
// phpcs:disable WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedFunctionFound


/**
 * Called  by the cron to pull in log files and store them.
 */
function s3_plc_cron_processor() {
	$bootstrap = new Bootstrap( plugin_dir_path( __FILE__ ) );
	$s3plc     = new LogCruncher();
	$bootstrap->cancel_wordpress_cron();
	set_time_limit( 0 );
	$s3plc->main();
}

// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
$s3plc_bootstrap = new Bootstrap( plugin_dir_path( __FILE__ ) );
$s3plc_bootstrap->init();

// phpcs:ignore WordPress.Security.NonceVerification.Missing
if ( isset( $_POST ) && isset( $_POST['save-changes'] ) ) {
	$s3plc_bootstrap->process_form();
}

// phpcs:ignore WordPress.Security.NonceVerification.Missing
if ( ( isset( $_POST ) && isset( $_POST['run-immediate'] ) ) ) {
	s3_plc_cron_processor();
}

